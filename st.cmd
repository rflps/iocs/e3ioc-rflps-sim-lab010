require essioc
require rflps_sim, develop

############################################################################
# ESS IOC configuration
############################################################################
epicsEnvSet(IOCNAME, "LabS-ICS:Ctrl-IOC-001")
epicsEnvSet(IOCDIR, "LabS-ICS1_Ctrl-IOC-001")
epicsEnvSet(LOG_SERVER_NAME, "172.30.4.43")
iocshLoad("$(essioc_DIR)/common_config.iocsh")

############################################################################
# RFLPS SIM Configuration
############################################################################
epicsEnvSet("PREFIX", "$(RFLPS_PREFIX=LAB-010RFC:)")
epicsEnvSet("PLCIP", "$(RFLPS_IP=172.30.4.74)")

#var s7plcDebug 5

## Datablocks
epicsEnvSet("TCPPORTCPU", "3000")
epicsEnvSet("PLCPORTCPU", "PLCCPU")
epicsEnvSet("INSIZECPU", "8")
epicsEnvSet("OUTSIZECPU", "4")

epicsEnvSet("TCPPORTAF", "3001")
epicsEnvSet("PLCPORTAF", "PLCAF")
epicsEnvSet("INSIZEAF", "1276")
epicsEnvSet("OUTSIZEAF", "754")

epicsEnvSet("TCPPORTDIO", "3002")
epicsEnvSet("PLCPORTDIO", "PLCDIO")
epicsEnvSet("INSIZEDIO", "1068")
epicsEnvSet("OUTSIZEDIO", "178")

epicsEnvSet("TCPPORTPSU", "3003")
epicsEnvSet("PLCPORTPSU", "PLCPSU")
epicsEnvSet("INSIZEPSU", "390")
epicsEnvSet("OUTSIZEPSU", "222")

s7plcConfigure("$(PLCPORTCPU)","$(PLCIP)",$(TCPPORTCPU),$(INSIZECPU),$(OUTSIZECPU),1,2500,500)
s7plcConfigure("$(PLCPORTAF)","$(PLCIP)",$(TCPPORTAF),$(INSIZEAF),$(OUTSIZEAF),1,2500,500)
s7plcConfigure("$(PLCPORTDIO)","$(PLCIP)",$(TCPPORTDIO),$(INSIZEDIO),$(OUTSIZEDIO),1,2500,500)
s7plcConfigure("$(PLCPORTPSU)","$(PLCIP)",$(TCPPORTPSU),$(INSIZEPSU),$(OUTSIZEPSU),1,2500,500)

dbLoadTemplate("$(E3_CMD_TOP)/subs/rflpsCPU.substitutions", "PREFIX=$(PREFIX)")
dbLoadTemplate("$(E3_CMD_TOP)/subs/rflpsAF.substitutions", "PREFIX=$(PREFIX)")
dbLoadTemplate("$(E3_CMD_TOP)/subs/rflpsDIO.substitutions", "PREFIX=$(PREFIX)")
dbLoadTemplate("$(E3_CMD_TOP)/subs/rflpsPSU.substitutions", "PREFIX=$(PREFIX)")

pvlistFromInfo simmetadata "mytest"

iocInit()
